package no.eriksendesign.edugame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class EndActivity extends AppCompatActivity {
    public final static String EXTRA_NAME = "no.eriksendesign.edugame.NAME";

    public Integer howManyCorrects ;
    public Integer howManyIncorrectly;
    public String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        Intent intent = getIntent();
        howManyCorrects = intent.getIntExtra(HowManyActivity.EXTRA_CORRECT, 0);
        howManyIncorrectly = intent.getIntExtra(HowManyActivity.EXTRA_INCORRECT, 0);
        name = intent.getStringExtra(HowManyActivity.EXTRA_GAME);

        Integer totalNumber = 6;
        String imageName;
        TextView correctNumberTextView = (TextView)findViewById(R.id.correctNumberTextView);


//        Calculate the percentage of how many corrects and show the correct star image
        Integer percent = (howManyCorrects * 100) / totalNumber;
        if (percent <= 17) {
            imageName = "stars_0";
        } else if (percent <= 33) {
            imageName = "stars_1";
        } else if (percent <= 66) {
            imageName = "stars_2";
        } else {
            imageName = "stars_3";
        }

        correctNumberTextView.setText(howManyCorrects.toString());
        ImageView starsImageView = (ImageView)findViewById(R.id.starsImageView);

        int rawId = getResources().getIdentifier(imageName, "drawable", getPackageName());
        starsImageView.setImageResource(rawId);
    }


    /** Called when the user clicks the level button */
    public void levelButton(View view) {
        if (name.equals("Counting")) {
            String name = "HowMany";
            Intent intent = new Intent(this, LevelsActivity.class);
            intent.putExtra(EXTRA_NAME, name);
            startActivity(intent);
        } else if (name.equals("Greater than")) {
            String name = "Biggest";
            Intent intent = new Intent(this, LevelsActivity.class);
            intent.putExtra(EXTRA_NAME, name);
            startActivity(intent);
        } else if (name.equals("Smaller than")) {
            String name = "Smallest";
            Intent intent = new Intent(this, LevelsActivity.class);
            intent.putExtra(EXTRA_NAME, name);
            startActivity(intent);
        } else if (name.equals("Addition")) {
            String name = "Addition";
            Intent intent = new Intent(this, LevelsActivity.class);
            intent.putExtra(EXTRA_NAME, name);
            startActivity(intent);
        } else if (name.equals("Subtracting")) {
            String name = "Subtracting";
            Intent intent = new Intent(this, LevelsActivity.class);
            intent.putExtra(EXTRA_NAME, name);
            startActivity(intent);
        }
    }

    /** Called when the user clicks the restart button. */
    public void restartButton(View view) {
        if (name.equals("Counting")) {
            Intent intent = new Intent(this, HowManyActivity.class);
            startActivity(intent);
        } else if (name.equals("Greater than")) {
            Intent intent = new Intent(this, BiggestNumberActivity.class);
            startActivity(intent);
        } else if (name.equals("Smaller than")) {
            Intent intent = new Intent(this, SmallestNumberActivity.class);
            startActivity(intent);
        } else if (name.equals("Addition")) {
            Intent intent = new Intent(this, AdditionActivity.class);
            startActivity(intent);
        } else if (name.equals("Subtracting")) {
            Intent intent = new Intent(this, SubtractingActivity.class);
            startActivity(intent);
        }
    }

    /** Called when the user clicks the home button */
    public void homeButton(View view) {
        Intent intent = new Intent(this, MainMenyActivity.class);
        startActivity(intent);
    }

}
