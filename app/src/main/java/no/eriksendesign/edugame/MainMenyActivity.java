package no.eriksendesign.edugame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainMenyActivity extends AppCompatActivity {
    public final static String EXTRA_NAME = "no.eriksendesign.edugame.NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_meny);
    }

    /** Called when the user clicks the numbers_button */
    public void numbersActivity(View view) {
        String name = "numbers";
        Intent intent = new Intent(this, LevelsActivity.class);
        intent.putExtra(EXTRA_NAME, name);
        startActivity(intent);
    }

    /** Called when the user clicks how_many_button */
    public void howManyActivity(View view) {
        String name = "HowMany";
        Intent intent = new Intent(this, LevelsActivity.class);
        intent.putExtra(EXTRA_NAME, name);
        startActivity(intent);
    }

    /** Called when the user clicks biggest_number_button */
    public void biggestNumberActivity(View view) {
        String name = "Biggest";
        Intent intent = new Intent(this, LevelsActivity.class);
        intent.putExtra(EXTRA_NAME, name);
        startActivity(intent);
    }

    /** Called when the user clicks smallest_number_button */
    public void smallestNumberActivity(View view) {
        String name = "Smallest";
        Intent intent = new Intent(this, LevelsActivity.class);
        intent.putExtra(EXTRA_NAME, name);
        startActivity(intent);
    }

    /** Called when the user clicks addition_button */
    public void additionActivity(View view) {
        String name = "Addition";
        Intent intent = new Intent(this, LevelsActivity.class);
        intent.putExtra(EXTRA_NAME, name);
        startActivity(intent);
    }

    /** Called when the user clicks subtracting_button */
    public void subtractingActivity(View view) {
        String name = "Subtracting";
        Intent intent = new Intent(this, LevelsActivity.class);
        intent.putExtra(EXTRA_NAME, name);
        startActivity(intent);
    }
}
