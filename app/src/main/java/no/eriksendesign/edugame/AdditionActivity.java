package no.eriksendesign.edugame;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AdditionActivity extends AppCompatActivity {
    public final static String EXTRA_CORRECT = "no.eriksendesign.edugame.CORRECT";
    public final static String EXTRA_INCORRECT = "no.eriksendesign.edugame.INCORRECT";
    public final static String EXTRA_GAME = "no.eriksendesign.edugame.GAME";
    public Integer correctAnswer = 4;
    public Integer howManyCorrects =  0;
    public Integer howManyIncorrectly = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addition);
    }



    public void answerButton1(View view) {
        //Get the button text value and parse it to a Integer
        TextView howManyButton1 = (TextView)findViewById(R.id.howManyButton1);
        Integer howManyButton1Text = Integer.parseInt(howManyButton1.getText().toString());

        //Get the correct and incorrect textview
        TextView correct = (TextView)findViewById(R.id.correctNumberTextView);
        TextView incorrect = (TextView)findViewById(R.id.incorrectNumberTextView);

        //Check if the answer is correct or not
        if (howManyButton1Text.equals(correctAnswer) ) {
            //howManyCorrects pluss 1 and update the TextView
            howManyCorrects++;
            correct.setText(howManyCorrects.toString());
            //Play the correct sound
            playNumberSound("correct");
            //Show the correct Toast
            correctToast();
        } else {
            //howManyIncorrectly pluss 1 and update the TextView
            howManyIncorrectly++;
            incorrect.setText(howManyIncorrectly.toString());
            //Play the incorrect sound
            playNumberSound("wrong");
            //Show the incorrect Toast
            incorrectToast();
        }
        //Change the activity content
        changeContent();
    }

    public void answerButton2(View view) {
        //Get the button text value and parse it to a Integer
        TextView howManyButton2 = (TextView)findViewById(R.id.howManyButton2);
        Integer howManyButton2Text = Integer.parseInt(howManyButton2.getText().toString());

        //Get the correct and incorrect textview
        TextView correct = (TextView)findViewById(R.id.correctNumberTextView);
        TextView incorrect = (TextView)findViewById(R.id.incorrectNumberTextView);

        //Check if the answer is correct or not
        if (howManyButton2Text.equals(correctAnswer) ) {
            //howManyCorrects pluss 1 and update the TextView
            howManyCorrects++;
            correct.setText(howManyCorrects.toString());
            //Play the correct sound
            playNumberSound("correct");
            //Show the correct Toast
            correctToast();
        } else {
            //howManyIncorrectly pluss 1 and update the TextView
            howManyIncorrectly++;
            incorrect.setText(howManyIncorrectly.toString());
            //Play the incorrect sound
            playNumberSound("wrong");
            //Show the incorrect Toast
            incorrectToast();
        }
        //Change the activity content
        changeContent();
    }

    public void answerButton3(View view) {
        //Get the button text value and parse it to a Integer
        TextView howManyButton3 = (TextView)findViewById(R.id.howManyButton3);
        Integer howManyButton3Text = Integer.parseInt(howManyButton3.getText().toString());

        //Get the correct and incorrect textview
        TextView correct = (TextView)findViewById(R.id.correctNumberTextView);
        TextView incorrect = (TextView)findViewById(R.id.incorrectNumberTextView);

        //Check if the answer is correct or not
        if (howManyButton3Text.equals(correctAnswer) ) {
            //howManyCorrects pluss 1 and update the TextView
            howManyCorrects++;
            correct.setText(howManyCorrects.toString());
            //Play the correct sound
            playNumberSound("correct");
            //Show the correct Toast
            correctToast();
        } else {
            //howManyIncorrectly pluss 1 and update the TextView
            howManyIncorrectly++;
            incorrect.setText(howManyIncorrectly.toString());
            //Play the incorrect sound
            playNumberSound("wrong");
            //Show the incorrect Toast
            incorrectToast();
        }
        //Change the activity content
        changeContent();
    }

    public void answerButton4(View view) {
        //Get the button text value and parse it to a Integer
        TextView howManyButton4 = (TextView)findViewById(R.id.howManyButton4);
        Integer howManyButton4Text = Integer.parseInt(howManyButton4.getText().toString());

        //Get the correct and incorrect textview
        TextView correct = (TextView)findViewById(R.id.correctNumberTextView);
        TextView incorrect = (TextView)findViewById(R.id.incorrectNumberTextView);

        //Check if the answer is correct or not
        if (howManyButton4Text.equals(correctAnswer) ) {
            //howManyCorrects pluss 1 and update the TextView
            howManyCorrects++;
            correct.setText(howManyCorrects.toString());
            //Play the correct sound
            playNumberSound("correct");
            //Show the correct Toast
            correctToast();
        } else {
            //howManyIncorrectly pluss 1 and update the TextView
            howManyIncorrectly++;
            incorrect.setText(howManyIncorrectly.toString());
            //Play the incorrect sound
            playNumberSound("wrong");
            //Show the incorrect Toast
            incorrectToast();
        }
        //Change the activity content
        changeContent();
    }

    public void answerButton5(View view) {
        //Get the button text value and parse it to a Integer
        TextView howManyButton5 = (TextView)findViewById(R.id.howManyButton5);
        Integer howManyButton5Text = Integer.parseInt(howManyButton5.getText().toString());

        //Get the correct and incorrect textview
        TextView correct = (TextView)findViewById(R.id.correctNumberTextView);
        TextView incorrect = (TextView)findViewById(R.id.incorrectNumberTextView);

        //Check if the answer is correct or not
        if (howManyButton5Text.equals(correctAnswer) ) {
            //howManyCorrects plus 1 and update the TextView
            howManyCorrects++;
            correct.setText(howManyCorrects.toString());
            //Play the correct sound
            playNumberSound("correct");
            //Show the correct Toast
            correctToast();
        } else {
            //howManyIncorrectly plus 1 and update the TextView
            howManyIncorrectly++;
            incorrect.setText(howManyIncorrectly.toString());
            //Play the incorrect sound
            playNumberSound("wrong");
            //Show the incorrect Toast
            incorrectToast();
        }
        //Change the activity content
        changeContent();
    }

    int counter = 0;
    public void changeContent() {
//        Counter plus one
        counter++;
//        declare variables
        String howManyButton1Text = "";
        String howManyButton2Text = "";
        String howManyButton3Text = "";
        String howManyButton4Text = "";
        String howManyButton5Text = "";
        String numberOne = "";
        String numberTwo = "";

//        Find the TextViews
        TextView numberOneTextView = (TextView)findViewById(R.id.numberOne);
        TextView numberTwoTextView = (TextView)findViewById(R.id.numberTwo);

//Find the buttons
        Button howManyButton1 = (Button)findViewById(R.id.howManyButton1);
        Button howManyButton2 = (Button)findViewById(R.id.howManyButton2);
        Button howManyButton3 = (Button)findViewById(R.id.howManyButton3);
        Button howManyButton4 = (Button)findViewById(R.id.howManyButton4);
        Button howManyButton5 = (Button)findViewById(R.id.howManyButton5);

//        A Switch that change the content of the activity
        switch (counter) {
            case 1:
                correctAnswer = 5;

                numberOne = "2";
                numberTwo = "3";

                howManyButton1Text = "5";
                howManyButton2Text = "2";
                howManyButton3Text = "3";
                howManyButton4Text = "4";
                howManyButton5Text = "9";
                break;
            case 2:
                correctAnswer = 9;

                numberOne = "5";
                numberTwo = "4";

                howManyButton1Text = "1";
                howManyButton2Text = "8";
                howManyButton3Text = "6";
                howManyButton4Text = "9";
                howManyButton5Text = "2";
                break;
            case 3:
                correctAnswer = 2;

                numberOne = "0";
                numberTwo = "2";

                howManyButton1Text = "2";
                howManyButton2Text = "10";
                howManyButton3Text = "8";
                howManyButton4Text = "5";
                howManyButton5Text = "1";
                break;
            case 4:
                correctAnswer = 4;

                numberOne = "1";
                numberTwo = "3";

                howManyButton1Text = "0";
                howManyButton2Text = "4";
                howManyButton3Text = "5";
                howManyButton4Text = "8";
                howManyButton5Text = "2";
                break;
            case 5:
                correctAnswer = 10;

                numberOne = "5";
                numberTwo = "5";

                howManyButton1Text = "10";
                howManyButton2Text = "0";
                howManyButton3Text = "8";
                howManyButton4Text = "1";
                howManyButton5Text = "4";
                break;
            default:
                String game = "Addition";
                Intent intent = new Intent(this, EndActivity.class);
                intent.putExtra(EXTRA_CORRECT, howManyCorrects);
                intent.putExtra(EXTRA_INCORRECT, howManyIncorrectly);
                intent.putExtra(EXTRA_GAME, game);
                startActivity(intent);
                break;
        }
        howManyButton1.setText(howManyButton1Text);
        howManyButton2.setText(howManyButton2Text);
        howManyButton3.setText(howManyButton3Text);
        howManyButton4.setText(howManyButton4Text);
        howManyButton5.setText(howManyButton5Text);

        numberOneTextView.setText(numberOne);
        numberTwoTextView.setText(numberTwo);
    }

    public void correctToast() {
        //Creating the LayoutInflater instance
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the correcttoast.xml file
        View layout = li.inflate(R.layout.correcttoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));

        //Creating the Toast object
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();
    }

    public void incorrectToast() {
        //Creating the LayoutInflater instance
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the incorrecttoast.xml file
        View layout = li.inflate(R.layout.incorrecttoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));

        //Creating the Toast object
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();
    }

    //    Play a sound when the sound button is pressed
    public void playNumberSound(String name) {
        int rawId = getResources().getIdentifier(name, "raw", getPackageName());
        final MediaPlayer mp = MediaPlayer.create(this, rawId);

        mp.start();
    }

    /** Called when the user clicks the home button */
    public void homeButton(View view) {
        Intent intent = new Intent(this, MainMenyActivity.class);
        startActivity(intent);
    }
}
