package no.eriksendesign.edugame;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class BiggestNumberActivity extends AppCompatActivity {
    public final static String EXTRA_CORRECT = "no.eriksendesign.edugame.CORRECT";
    public final static String EXTRA_INCORRECT = "no.eriksendesign.edugame.INCORRECT";
    public final static String EXTRA_GAME = "no.eriksendesign.edugame.GAME";
    public Integer correctAnswer = 4;
    public Integer howManyCorrects =  0;
    public Integer howManyIncorrectly = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biggest_number);
    }

    public void numberOne(View view) {
        //Get the button text value and parse it to a Integer
        Button numberOneButton = (Button)findViewById(R.id.numberOneButton);
        Integer numberOneButtonText = Integer.parseInt(numberOneButton.getText().toString());
        //Get the button text value and parse it to a Integer
        Button numberTwoButton = (Button)findViewById(R.id.numberTwoButton);
        Integer numberTwoButtonText = Integer.parseInt(numberTwoButton.getText().toString());
        //Get the correct and incorrect textview
        TextView correct = (TextView)findViewById(R.id.correctNumberTextView);
        TextView incorrect = (TextView)findViewById(R.id.incorrectNumberTextView);

        //Check if the answer is correct or not
        if (numberOneButtonText > numberTwoButtonText ) {
            //howManyCorrects pluss 1 and update the TextView
            howManyCorrects++;
            correct.setText(howManyCorrects.toString());
            //Play the correct sound
            playNumberSound("correct");
            //Show the correct Toast
            correctToast();
        } else {
            //howManyIncorrectly pluss 1 and update the TextView
            howManyIncorrectly++;
            incorrect.setText(howManyIncorrectly.toString());
            //Play the incorrect sound
            playNumberSound("wrong");
            //Show the incorrect Toast
            incorrectToast();
        }
        //Change the activity content
        changeContent();
    }

    public void buttonTwo(View view) {
        //Get the button text value and parse it to a Integer
        Button numberOneButton = (Button)findViewById(R.id.numberOneButton);
        Integer numberOneButtonText = Integer.parseInt(numberOneButton.getText().toString());
        //Get the button text value and parse it to a Integer
        Button numberTwoButton = (Button)findViewById(R.id.numberTwoButton);
        Integer numberTwoButtonText = Integer.parseInt(numberTwoButton.getText().toString());
        //Get the correct and incorrect textview
        TextView correct = (TextView)findViewById(R.id.correctNumberTextView);
        TextView incorrect = (TextView)findViewById(R.id.incorrectNumberTextView);

        //Check if the answer is correct or not
        if (numberTwoButtonText > numberOneButtonText ) {
            //howManyCorrects pluss 1 and update the TextView
            howManyCorrects++;
            correct.setText(howManyCorrects.toString());
            //Play the correct sound
            playNumberSound("correct");
            //Show the correct Toast
            correctToast();
        } else {
            //howManyIncorrectly pluss 1 and update the TextView
            howManyIncorrectly++;
            incorrect.setText(howManyIncorrectly.toString());
            //Play the incorrect sound
            playNumberSound("wrong");
            //Show the incorrect Toast
            incorrectToast();
        }
        //Change the activity content
        changeContent();
    }

    int counter = 0;
    public void changeContent() {
        //        Counter plus one

        counter++;
//Find the number buttons
        Button numberOneButton = (Button)findViewById(R.id.numberOneButton);
        Button numberTwoButton = (Button)findViewById(R.id.numberTwoButton);
//        declare variables
        String numberOne = "";
        String numberTwo = "";

        // A Switch that change the content of the activity
        switch (counter) {
            case 1:
                correctAnswer = 3;

                numberOne = "2";
                numberTwo = "3";

                break;
            case 2:
                correctAnswer = 9;

                numberOne = "9";
                numberTwo = "3";

                break;
            case 3:
                correctAnswer = 5;

                numberOne = "2";
                numberTwo = "5";

                break;
            case 4:
                correctAnswer = 10;

                numberOne = "10";
                numberTwo = "3";

                break;
            case 5:
                correctAnswer = 3;

                numberOne = "3";
                numberTwo = "1";

                break;
            default:
                String game = "Greater than";
                Intent intent = new Intent(this, EndActivity.class);
                intent.putExtra(EXTRA_CORRECT, howManyCorrects);
                intent.putExtra(EXTRA_INCORRECT, howManyIncorrectly);
                intent.putExtra(EXTRA_GAME, game);
                startActivity(intent);
                break;
        }

        numberOneButton.setText(numberOne);
        numberTwoButton.setText(numberTwo);
    }

    public void correctToast() {
        //Creating the LayoutInflater instance
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the correcttoast.xml file
        View layout = li.inflate(R.layout.correcttoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));

        //Creating the Toast object
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();
    }

    public void incorrectToast() {
        //Creating the LayoutInflater instance
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the incorrecttoast.xml file
        View layout = li.inflate(R.layout.incorrecttoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));

        //Creating the Toast object
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.FILL_HORIZONTAL, 0, 0);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();
    }

    //    Play a sound when the sound button is pressed
    public void playNumberSound(String name) {
        int rawId = getResources().getIdentifier(name, "raw", getPackageName());
        final MediaPlayer mp = MediaPlayer.create(this, rawId);

        mp.start();
    }

    /** Called when the user clicks the home button */
    public void homeButton(View view) {
        Intent intent = new Intent(this, MainMenyActivity.class);
        startActivity(intent);
    }
}
