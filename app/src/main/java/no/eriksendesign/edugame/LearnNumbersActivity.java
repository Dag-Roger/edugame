package no.eriksendesign.edugame;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class LearnNumbersActivity extends AppCompatActivity {
    public final static String EXTRA_NAME = "no.eriksendesign.edugame.NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_numbers);

//        Play the sound file that says One
        playNumberSound("one");
//        Make so the sound button is ready to play the sound file taht says One
        playNumberSoundButton("one");
    }

    int counter = 1;
    public void nextNumber(View view) {
        //        Counter plus one
        counter++;
        //        declare variables

        String number = "";
        String numberText = "";
        String imageName = "";

//        Find the TextViews and ImangeView
        TextView numbersTextTextView = (TextView)findViewById(R.id.numbersText);
        TextView numbersTextView = (TextView)findViewById(R.id.bigNumber);
        ImageView numberImageView = (ImageView)findViewById(R.id.numbersAnimalsImageView);

//        A Switch that change the content of the activity
        switch (counter) {
            case 1:
                numberText = "One";
                number = "1";
                break;
            case 2:
                numberText = "Two";
                number = "2";
                playNumberSound("two");
                playNumberSoundButton("two");
                imageName = "animals_two";
                break;
            case 3:
                numberText = "Three";
                number = "3";
                playNumberSound("three");
                playNumberSoundButton("three");
                imageName = "animals_three";
                break;
            case 4:
                numberText = "Four";
                number = "4";
                playNumberSound("four");
                playNumberSoundButton("four");
                imageName = "animals_four";
                break;
            case 5:
                numberText = "Five";
                number = "5";
                playNumberSound("five");
                playNumberSoundButton("five");
                imageName = "animals_five";
                break;
            case 6:
                numberText = "Six";
                number = "6";
                playNumberSound("six");
                playNumberSoundButton("six");
                imageName = "animals_six";
                break;
            case 7:
                numberText = "Seven";
                number = "7";
                playNumberSound("seven");
                playNumberSoundButton("seven");
                imageName = "animals_seven";
                break;
            case 8:
                numberText = "Eight";
                number = "8";
                playNumberSound("eight");
                playNumberSoundButton("eight");
                imageName = "animals_eight";
                break;
            case 9:
                numberText = "Nine";
                number = "9";
                playNumberSound("nine");
                playNumberSoundButton("nine");
                imageName = "animals_nine";
                break;
            case 10:
                numberText = "Ten";
                number = "10";
                playNumberSound("ten");
                playNumberSoundButton("ten");
                imageName = "animals_ten";
                break;
            default:
                String name = "numbers";
                Intent intent = new Intent(this, LevelsActivity.class);
                intent.putExtra(EXTRA_NAME, name);
                startActivity(intent);
                break;
        }
        numbersTextTextView.setText(numberText);
        numbersTextView.setText(number);

        int rawId = getResources().getIdentifier(imageName, "drawable", getPackageName());
        numberImageView.setImageResource(rawId);
    }

    /** Called when the user clicks the home button */
    public void homeButton(View view) {
        Intent intent = new Intent(this, MainMenyActivity.class);
        startActivity(intent);
    }

    //    Play number sound when the sound button is pressed
    public void playNumberSoundButton(String number) {
        int rawId = getResources().getIdentifier(number, "raw", getPackageName());
        final MediaPlayer mp = MediaPlayer.create(this, rawId);

        ImageButton play_button = (ImageButton)this.findViewById(R.id.soundButton);
        play_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
            }
        });
    }

    //    Play number sound when called
    public void playNumberSound(String number) {
        int rawId = getResources().getIdentifier(number, "raw", getPackageName());
        final MediaPlayer mp = MediaPlayer.create(this, rawId);

        mp.start();
    }

}
