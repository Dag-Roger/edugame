package no.eriksendesign.edugame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class LevelsActivity extends AppCompatActivity {

    String name = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels);

        Intent intent = getIntent();
        name = intent.getStringExtra(MainMenyActivity.EXTRA_NAME);

//        Set the background color to match the color scheme in that game
        if (name.equals("numbers")) {
            Button levelOne = (Button)findViewById(R.id.levelOne);
            levelOne.setBackgroundResource(R.drawable.numbersbutton_one);
        } else if (name.equals("HowMany")) {
            Button levelOne = (Button)findViewById(R.id.levelOne);
            levelOne.setBackgroundResource(R.drawable.countingbutton_one);
        } else if (name.equals("Biggest")) {
            Button levelOne = (Button)findViewById(R.id.levelOne);
            levelOne.setBackgroundResource(R.drawable.greaterthanbutton_one);
        } else if (name.equals("Smallest")) {
            Button levelOne = (Button)findViewById(R.id.levelOne);
            levelOne.setBackgroundResource(R.drawable.smallerthanbutton_one);
        } else if (name.equals("Addition")) {
            Button levelOne = (Button)findViewById(R.id.levelOne);
            levelOne.setBackgroundResource(R.drawable.additionbutton_one);
        } else if (name.equals("Subtracting")) {
            Button levelOne = (Button)findViewById(R.id.levelOne);
            levelOne.setBackgroundResource(R.drawable.subtractingbutton_one);
        }

    }


    /** Called when the user clicks the button */
    public void level(View view) {
        if (name.equals("numbers")) {
            Intent intent = new Intent(this, LearnNumbersActivity.class);
            startActivity(intent);
        } else if (name.equals("HowMany")) {
            Intent intent = new Intent(this, HowManyActivity.class);
            startActivity(intent);
        } else if (name.equals("Biggest")) {
            Intent intent = new Intent(this, BiggestNumberActivity.class);
            startActivity(intent);
        } else if (name.equals("Smallest")) {
            Intent intent = new Intent(this, SmallestNumberActivity.class);
            startActivity(intent);
        } else if (name.equals("Addition")) {
            Intent intent = new Intent(this, AdditionActivity.class);
            startActivity(intent);
        } else if (name.equals("Subtracting")) {
            Intent intent = new Intent(this, SubtractingActivity.class);
            startActivity(intent);
        }

    }

    /** Called when the user clicks the home button */
    public void homeButton(View view) {
        Intent intent = new Intent(this, MainMenyActivity.class);
        startActivity(intent);
    }
}
